package rair2184MV.repository;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.model.Intrebare;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.util.InputValidation;

public class IntrebariRepository {

	private List<Intrebare> intrebari;
	
	public IntrebariRepository() {
		setIntrebari(new LinkedList<Intrebare>());
	}
	
	public void adaugaIntrebare(Intrebare i,String fis) throws DuplicateIntrebareException, InputValidationFailedException {
		if(exista(i))
			throw new DuplicateIntrebareException("Intrebarea deja exista!");
		try{
			InputValidation.valideazaDomeniu(i.getDomeniu());
			InputValidation.valideazaVarianta1(i.getVarianta1());
			InputValidation.valideazaVarianta2(i.getVarianta2());
			InputValidation.valideazaVarianta3(i.getVarianta3());
			InputValidation.valideazaVariantaCorecta(i.getVariantaCorecta());
			InputValidation.valideazaEnunt(i.getEnunt());
			intrebari.add(i);
			try(FileWriter fw = new FileWriter(fis, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw))
			{
				out.println(i.getEnunt()+";"+i.getVarianta1()+";"+i.getVarianta2()+";"+i.getVarianta3()+";"+i.getVariantaCorecta()+";"+i.getDomeniu());
			} catch (IOException e) {
				throw new DuplicateIntrebareException("Probleme");
			}
		}catch (InputValidationFailedException ed){
			throw new InputValidationFailedException("Intrebarea nu are format corespunzator!");
		}
	}
	
	public boolean exista(Intrebare i){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(i))
				return true;
		return false;
	}
	
	public Intrebare alegeRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumarDomeniiDistincte(){
		return getDomeniiDistincte().size();
		
	}
	
	public Set<String> getDomeniiDistincte(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariDupaDomeniu(String domain){
		List<Intrebare> intrebariDupaDomeniu = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariDupaDomeniu.add(intrebare);
			}
		}
		
		return intrebariDupaDomeniu;
	}
	
	public int getNumarDeIntrebariDupaDomeniu(String domain){
		return getIntrebariDupaDomeniu(domain).size();
	}
	
	public List<Intrebare> incarcaIntrebariDinFisier(String f) throws Exception {
		
		//List<Intrebare> intrebari = new LinkedList<Intrebare>();
		BufferedReader br = null; 
		String linie = null;
		//List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			br = new BufferedReader(new FileReader(new File(f)));
			linie = br.readLine();
			while(linie != null){
				//intrebareAux = new LinkedList<String>();
				//while(!line.equals("##")){
					//intrebareAux.add(line);
					//line = br.readLine();
				//}
				try{ String[] intrebareAux=linie.split(";");
					InputValidation.valideazaEnunt(intrebareAux[0]);
				InputValidation.valideazaVarianta1(intrebareAux[1]);
				InputValidation.valideazaVarianta2(intrebareAux[2]);
				InputValidation.valideazaVarianta3(intrebareAux[3]);
				InputValidation.valideazaVariantaCorecta(intrebareAux[4]);
				InputValidation.valideazaDomeniu(intrebareAux[5]);
				intrebare = new Intrebare();
				intrebare.setEnunt(intrebareAux[0]);
				intrebare.setVarianta1(intrebareAux[1]);
				intrebare.setVarianta2(intrebareAux[2]);
				intrebare.setVarianta3(intrebareAux[3]);
				intrebare.setVariantaCorecta(intrebareAux[4]);
				intrebare.setDomeniu(intrebareAux[5]);
				intrebari.add(intrebare); }catch(InputValidationFailedException ed){
					System.out.println("Probleme validare citire");
				}
				linie = br.readLine();
			}
		
		}
		catch (IOException e) {
			throw new Exception(e.getMessage());
		}
		finally{
			try {
				br.close();
			} catch (IOException e) {
				throw new Exception(e.getMessage());
			}
		}
		
		return intrebari;
	}
	
	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
