package rair2184MV.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.exception.NotAbleToCreateStatisticsException;
import rair2184MV.model.Intrebare;
import rair2184MV.model.Statistica;
import rair2184MV.repository.IntrebariRepository;
import rair2184MV.util.InputValidation;

import java.io.PrintWriter;

import static org.junit.Assert.*;

public class AppControllerTestStatistica {
    private String file="intrebStati.txt";
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;

    @Before
    public void setUp() throws Exception {
        this.intrebariRepository = new IntrebariRepository();
        this.inputValidation = new InputValidation();
        this.appController = new AppController();
        appController.incarcaIntrebariDinFisier(file);
    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test(expected = rair2184MV.exception.NotAbleToCreateStatisticsException.class)
    public void tc_nonvalid() throws NotAbleToCreateStatisticsException {
        appController.getStatistica();
    }
    @Test
    public void tc_valid() throws NotAbleToCreateStatisticsException, DuplicateIntrebareException, InputValidationFailedException {

        Intrebare intrebare1 = new Intrebare("Ce simbol este Ca?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1", "Biologie");
        Intrebare intrebare2 = new Intrebare("Ce e genotipul?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Biologie");
        Intrebare intrebare3 = new Intrebare("Cat face 2+4?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Matematica");

        appController.adaugaIntrebareNoua(intrebare1,file);
        appController.adaugaIntrebareNoua(intrebare2,file);
        appController.adaugaIntrebareNoua(intrebare3,file);
        Statistica s = appController.getStatistica();
        //Assert.assertEquals(s.getIntrebariDomenii().size(),2);
        Assert.assertTrue(s.getIntrebariDomenii().size()>0);
    }
}