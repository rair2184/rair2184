package rair2184MV.controller;

import org.junit.After;
import org.junit.Before;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.exception.NotAbleToCreateTestException;
import rair2184MV.model.Intrebare;
import rair2184MV.repository.IntrebariRepository;
import rair2184MV.util.InputValidation;

import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;

public class AppControllerTest {
    private String file="intrbTestc.txt";
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;

    @Before
    public void setUp() throws Exception {
        this.intrebariRepository = new IntrebariRepository();
        this.inputValidation = new InputValidation();
        this.appController = new AppController();
        appController.incarcaIntrebariDinFisier(file);
        Intrebare intrebare1 = new Intrebare("Ce e pisica?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1", "Biologie");
        Intrebare intrebare2 = new Intrebare("Cat face 2*3?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Matematica");

        appController.adaugaIntrebareNoua(intrebare1,file);
        appController.adaugaIntrebareNoua(intrebare2,file);
    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @org.junit.Test(expected = rair2184MV.exception.NotAbleToCreateTestException.class)
    public void tc1() throws NotAbleToCreateTestException {
        appController.creazaTestNou();
    }

    @org.junit.Test(expected = rair2184MV.exception.NotAbleToCreateTestException.class)
    public void tc2() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Ce simbol este Ca?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1", "Biologie");
        Intrebare intrebare2 = new Intrebare("Ce e genotipul?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Biologie");
        Intrebare intrebare3 = new Intrebare("Cat face 2+4?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Matematica");

        appController.adaugaIntrebareNoua(intrebare1,file);
        appController.adaugaIntrebareNoua(intrebare2,file);
        appController.adaugaIntrebareNoua(intrebare3,file);
        appController.creazaTestNou();
    }

    @org.junit.Test
    public void tc3() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Ce simbol este Ca?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1", "Chimie");
        Intrebare intrebare2 = new Intrebare("Ce formula are lucrul mecanic?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Fizica");
        Intrebare intrebare3 = new Intrebare("In ce an a inceput WWI?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Istorie");

        appController.adaugaIntrebareNoua(intrebare1,file);
        appController.adaugaIntrebareNoua(intrebare2,file);
        appController.adaugaIntrebareNoua(intrebare3,file);

        rair2184MV.model.Test t = appController.creazaTestNou();
        assertEquals(t.getIntrebari().size(),5);
    }
}