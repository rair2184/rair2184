package rair2184MV.inter;

import rair2184MV.controller.AppController;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.exception.NotAbleToCreateStatisticsException;
import rair2184MV.exception.NotAbleToCreateTestException;
import rair2184MV.model.Intrebare;
import rair2184MV.model.Statistica;
import rair2184MV.model.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Interface {
    private AppController appController;
    public BufferedReader bufferedReader;
    private static final String fisier = "intrebari.txt";

    public Interface(AppController appController) {
        this.appController = appController;
        try {
            appController.incarcaIntrebariDinFisier(fisier);
        } catch (Exception e) {
            System.out.println("Probleme la citire fisier");
        }
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    }

    private void menu() {
        System.out.println("");
        System.out.println("1.Adauga intrebare");
        System.out.println("2.Creeaza test");
        System.out.println("3.Statistica");
        System.out.println("4.Exit");
        System.out.println("");
    }

    public void show() {
        String cmd;

        menu();


        while (true) {
            try {
                cmd = bufferedReader.readLine();
                if (cmd.equals("4"))
                    break;
                else if (cmd.equals("1")) {
                    try {
                        System.out.println("Da domeniu:");
                        String d;
                        d=bufferedReader.readLine();
                        System.out.println("Da enunt:");
                        String e;
                        e=bufferedReader.readLine();
                        System.out.println("Da varianta1:");
                        String v1;
                        v1=bufferedReader.readLine();
                        System.out.println("Da varianta2:");
                        String v2;
                        v2=bufferedReader.readLine();
                        System.out.println("Da varianta3:");
                        String v3;
                        v3=bufferedReader.readLine();
                        System.out.println("Da varianta corecta (1, 2 sau 3):");
                        String vc;
                        vc=bufferedReader.readLine();


                        try {
                            Intrebare i= new Intrebare(e,v1,v2,v3,vc,d);
                            if (appController.adaugaIntrebareNoua(i,"intrebari.txt") !=null)
                                System.out.println("Adaugata");
                            else
                                System.out.println("Nu a fost adaugata");
                        } catch (InputValidationFailedException e1) {
                            System.out.println(e1.getMessage());
                        } catch (DuplicateIntrebareException e1) {
                            System.out.println(e1.getMessage());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (cmd.equals("2")) {
                    try {
                        Test t=appController.creazaTestNou();
                        System.out.println("Testul este:" + t.toString());
                    } catch (NotAbleToCreateTestException ex) {
                        System.out.println(ex.getMessage());
                    }

                } else if (cmd.equals("3")) {
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println("Statistica este: "+statistica);
                    } catch (NotAbleToCreateStatisticsException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
