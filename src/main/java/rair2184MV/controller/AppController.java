package rair2184MV.controller;

import java.util.LinkedList;
import java.util.List;

import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.model.Intrebare;
import rair2184MV.model.Statistica;
import rair2184MV.model.Test;
import rair2184MV.repository.IntrebariRepository;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.NotAbleToCreateStatisticsException;
import rair2184MV.exception.NotAbleToCreateTestException;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	
	public AppController() {
		intrebariRepository = new IntrebariRepository();
	}
	
	public Intrebare adaugaIntrebareNoua(Intrebare intrebare,String fis) throws DuplicateIntrebareException, InputValidationFailedException {
		try {
			intrebariRepository.adaugaIntrebare(intrebare,fis);
		}catch (DuplicateIntrebareException ef){
			throw new DuplicateIntrebareException("Intrebarea exista deja");
		}
		catch (InputValidationFailedException ef){
			throw new InputValidationFailedException("Intrebarea nu are format corespunzator");
		}
		
		return intrebare;
	}
	
	public boolean exista(Intrebare intrebare){
		return intrebariRepository.exista(intrebare);
	}
	
	public Test creazaTestNou() throws NotAbleToCreateTestException{
		
		if(intrebariRepository.getIntrebari().size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		
		if(intrebariRepository.getNumarDomeniiDistincte() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");
		
		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();
		
		while(testIntrebari.size() != 5){
			intrebare = intrebariRepository.alegeRandomIntrebare();
			
			if(!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())){
				testIntrebari.add(intrebare);
				domenii.add(intrebare.getDomeniu());
			}
			
		}
		
		test.setIntrebari(testIntrebari);
		return test;
		
	}
	
	public void incarcaIntrebariDinFisier(String f) throws Exception {
		try {
			intrebariRepository.setIntrebari(intrebariRepository.incarcaIntrebariDinFisier(f));
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDomeniiDistincte()){
			//statistica.add(domeniu, intrebariRepository.getIntrebari().size());
			statistica.add(domeniu, intrebariRepository.getIntrebariDupaDomeniu(domeniu).size());
		}
		
		return statistica;
	}

}
