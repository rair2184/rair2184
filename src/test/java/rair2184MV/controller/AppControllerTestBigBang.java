package rair2184MV.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.exception.NotAbleToCreateStatisticsException;
import rair2184MV.exception.NotAbleToCreateTestException;
import rair2184MV.inter.Interface;
import rair2184MV.main.StartApp;
import rair2184MV.model.Intrebare;
import rair2184MV.model.Statistica;
import rair2184MV.repository.IntrebariRepository;
import rair2184MV.util.InputValidation;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AppControllerTestBigBang {
    private String file;
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;
    private Interface ap;


    @Before
    public void setUp() throws Exception {
        file = "intrebari.txt";
        this.intrebariRepository = new IntrebariRepository();
        this.inputValidation = new InputValidation();
        this.appController = new AppController();
        ap = new Interface(appController);
        ap.bufferedReader = Mockito.mock(BufferedReader.class);

    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test(expected = rair2184MV.exception.NotAbleToCreateTestException.class)
    public void unit2B()throws NotAbleToCreateTestException {
        appController.creazaTestNou();
    }

    @Test(expected = rair2184MV.exception.InputValidationFailedException.class)
    public void unit1A()throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cat face 2*3??", "1)1", "2)6", "3)5", "4", "Matematica");
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");

    }
    @Test
    public void unit3C() throws NotAbleToCreateStatisticsException, DuplicateIntrebareException, InputValidationFailedException {

        Intrebare intrebare1 = new Intrebare("Ce simbol este Ca?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1", "Biologie");
        Intrebare intrebare2 = new Intrebare("Ce e genotipul?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Biologie");
        Intrebare intrebare3 = new Intrebare("Cat face 2+4?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "2", "Matematica");

        appController.adaugaIntrebareNoua(intrebare1,file);
        appController.adaugaIntrebareNoua(intrebare2,file);
        appController.adaugaIntrebareNoua(intrebare3,file);
        Statistica s = appController.getStatistica();
        //Assert.assertEquals(s.getIntrebariDomenii().size(),2);
        Assert.assertTrue(s.getIntrebariDomenii().size()>0);
    }

    @Test
    public void integration_test() {

        try {


            ap.bufferedReader = Mockito.mock(BufferedReader.class);

            Mockito.when(ap.bufferedReader.readLine()).thenReturn("1", "Biologie","Care e celula cea mai putin raspandita?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1",
                    "1", "Matematica", "Care e celula cea mai putin raspandita?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1",
                    "1", "Fizica", "Care e celula cea mai putin raspandita?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1",
                    "1", "Chimie", "Care e celula cea mai putin raspandita?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1",
                    "1", "Fizpat", "Care e celula cea mai putin raspandita?", "1)Raspuns unu.", "2)Raspuns doi.", "3)Raspuns trei.", "1",
                    "2",
                    "3",
                    "4");
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            System.setOut(new PrintStream(bo));
            bo.flush();
            ap.show();
            String allWrittenLines = new String(bo.toByteArray());

            assertTrue(allWrittenLines.contains("Adaugata"));
            assertTrue(allWrittenLines.contains("Testul este:"));
            assertTrue(appController.getStatistica().getIntrebariDomenii().size() > 0);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }


    }
}