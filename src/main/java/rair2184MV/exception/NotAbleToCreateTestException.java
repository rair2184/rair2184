package rair2184MV.exception;

public class NotAbleToCreateTestException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NotAbleToCreateTestException(String message) {
		super(message);
	}

}
