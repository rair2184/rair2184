package rair2184MV.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.exception.NotAbleToCreateTestException;
import rair2184MV.inter.Interface;
import rair2184MV.model.Intrebare;
import rair2184MV.model.Statistica;

import rair2184MV.controller.AppController;
import rair2184MV.exception.NotAbleToCreateStatisticsException;
import rair2184MV.model.Test;
import rair2184MV.repository.IntrebariRepository;
import rair2184MV.util.InputValidation;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String fisier = "intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		//BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		try {
			IntrebariRepository intrebariRepository = new IntrebariRepository();
			InputValidation inputValidation = new InputValidation();
			AppController appController = new AppController();
			Interface userInterface = new Interface(appController);
			userInterface.show();
		} catch (Exception e) {
		e.printStackTrace();
		}
		/*AppController appController = new AppController();
		try {
			appController.incarcaIntrebariDinFisier(fisier);
		} catch (Exception e) {
			System.out.println("Probleme la citire fisier");
		}
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();

			switch(optiune){
			case "1" :
				System.out.println("Da domeniu:");
				String d;
				d=console.readLine();
				System.out.println("Da enunt:");
				String e;
				e=console.readLine();
				System.out.println("Da varianta1:");
				String v1;
				v1=console.readLine();
				System.out.println("Da varianta2:");
				String v2;
				v2=console.readLine();
				System.out.println("Da varianta3:");
				String v3;
				v3=console.readLine();
				System.out.println("Da varianta corecta (1, 2 sau 3):");
				String vc;
				vc=console.readLine();
				try {
					Intrebare i= new Intrebare(e,v1,v2,v3,vc,d);
					appController.adaugaIntrebareNoua(i,"intrebari.txt");
				} catch (InputValidationFailedException e1) {
					System.out.println(e1.getMessage());
				} catch (DuplicateIntrebareException e1) {
					System.out.println(e1.getMessage());
				}
				break;
			case "2" :
				try {
					Test t=appController.creazaTestNou();
					for(Intrebare i:t.getIntrebari()){
						System.out.println("Domeniu: "+i.getDomeniu()+"\n"+"Intrebare: "+i.getEnunt()+"\n"+i.getVarianta1()+"\n"+i.getVarianta2()+"\n"+i.getVarianta3());
					}
				} catch (NotAbleToCreateTestException ex) {
					System.out.println(ex.getMessage());
				}
				break;
			case "3" :

				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException ex) {
					System.out.println(ex.getMessage());
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}*/
		
	}

}
