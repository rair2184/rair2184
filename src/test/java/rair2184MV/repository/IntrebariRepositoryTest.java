package rair2184MV.repository;

import org.junit.Before;
import org.junit.Test;
import rair2184MV.exception.DuplicateIntrebareException;
import rair2184MV.exception.InputValidationFailedException;
import rair2184MV.model.Intrebare;
import rair2184MV.util.InputValidation;

import static org.junit.Assert.*;

public class IntrebariRepositoryTest {
    private IntrebariRepository intrebariRepository;
    private InputValidation inputValidation;


    @Before
    public void setUp() throws Exception {
        this.intrebariRepository = new IntrebariRepository();
        this.inputValidation = new InputValidation();
    }

    @Test
    public void ecp_valid() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cat face 2+2?", "1)4", "2)5", "3)3", "1", "Matematica");
        int size = intrebariRepository.getIntrebari().size();
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
        assertEquals(intrebariRepository.getIntrebari().size(), size+1);
    }

    @Test
    public void ecp_valid2() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Ce e cainele?", "1)Insecta", "2)Mamifer", "3)Peste", "2", "Biologie");
        int size = intrebariRepository.getIntrebari().size();
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
        assertEquals(intrebariRepository.getIntrebari().get(size).getEnunt(), "Ce e cainele?");
    }

    @Test(expected = rair2184MV.exception.InputValidationFailedException.class)
    public void ecp_invalid_raspCorect() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Cat face 2*3??", "1)1", "2)6", "3)5", "4", "Matematica");
       intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
    }

    @Test(expected = rair2184MV.exception.InputValidationFailedException.class)
    public void ecp_invalid_enunt() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee e pisica?", "1)Mamifer", "2)Reptila", "3)Peste", "1", "Biologie");
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
    }

    @Test
    public void bva_valid() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("C", "1)1", "2)2", "3)3", "1", "Fizica");
        int size = intrebariRepository.getIntrebari().size();
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
        assertEquals(intrebariRepository.getIntrebari().size(), size+1);
    }
    @Test
    public void bva_valid2() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "1)!", "2)2", "3)3", "1", "Fizica");
        int size = intrebariRepository.getIntrebari().size();
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
        assertEquals(intrebariRepository.getIntrebari().get(size).getDomeniu(), "Fizica");
    }

    @Test(expected = rair2184MV.exception.InputValidationFailedException.class)
    public void bva_invalid_enunt() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("", "1)1", "2)2", "3)3", "1", "Fizica");
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
    }

    @Test(expected = rair2184MV.exception.InputValidationFailedException.class)
    public void bva_invalid_raspCorect() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Ce e vaca?", "1)Mamifer", "2)Peste", "3)3", "4", "Biologie");
        intrebariRepository.adaugaIntrebare(intrebare,"intrebari.txt");
    }
}